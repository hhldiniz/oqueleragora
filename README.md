# O que ler Agora? #
Aplicativo para Android.

Quer ler a Bíblia, mas não sabe por onde começar? O aplicativo escolhe para você! Basta escolher uma opção de busca e apertar o botão!

### O que será necessário ? ###

* Android Studio
* Biblioteca Kotlin para IntelliJ IDEA
* Plugin adicional 'android-kotlin-extensions' para Gradle
* Dispositivo alvo ou emulador com API Level 21 ou superior (Você pode modificar essas configurações no projeto se achar necessário)

### FONTE DE DADOS ###
O Banco de Dados usado para essa aplicação é de terceiros e poderá ser obtido no projeto disponível no seguinte link:
https://github.com/thiagobodruk/biblia. Aqui foi usada apenas a versão Almeida Revisada Imprensa Bíblica, mas você poderá encontrar outras no projeto citado.
A tradução bíblica Almeida Revisada Imprensa Bíblica é propriedade da Imprensa Bíblica Brasileira. Todos os direitos reservados.


### Distribuição ###

Esse é um software livre. Você pode copiá-lo, distribuí-lo, modificá-lo e usar como referencia para outros projetos da maneira que achar
conveniente. Apenas peço que coloque um link em algum lugar do seu projeto para o projeto original. ( Esse aqui :D ) Não esqueça também de referenciar
o projeto da fonte de dados desse aplicativo citado acima.