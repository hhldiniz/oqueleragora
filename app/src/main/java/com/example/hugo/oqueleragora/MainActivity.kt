package com.example.hugo.oqueleragora

import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.CompoundButton
import android.widget.Toast
import kotlinx.android.synthetic.main.main_layout.*
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity(), View.OnClickListener,CompoundButton.OnCheckedChangeListener {
    private val livros = Hashtable<String,Int>()
    private val listaLivros = ArrayList<String>()
    private lateinit var prefs : SharedPreferences
    private lateinit var editor : SharedPreferences.Editor
    private lateinit var bancoController : BancoController
    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        if(buttonView as Button? == switch_livros && isChecked)
        {
            switch_ambos.isChecked=false
        }
        if(buttonView as Button? == switch_ambos && isChecked)
        {
            switch_livros.isChecked=false
        }
    }
    override fun onClick(v: View?) {
       when(v)
       {
           button ->
           {
               when {
                   switch_ambos.isChecked -> {
                       val tamanhoLista : Int = listaLivros.size
                       val aux : Int = Random().nextInt(tamanhoLista)
                       result_livro!!.text = listaLivros[aux]
                       result_capitulo!!.text = (Random().nextInt(livros[result_livro!!.text.toString()]!!)+1).toString()
                       bancoController.abrirPassagemTodos(result_livro.text.toString(),result_capitulo.text.toString().toInt())
                   }
                   switch_livros.isChecked -> {
                       result_capitulo!!.text = ""
                       val tamanhoLista : Int = listaLivros.size
                       val aux : Int = Random().nextInt(tamanhoLista)
                       result_livro!!.text = listaLivros[aux]
                       bancoController.abrirPassagemLivro(result_livro.text.toString())
                   }
                   else -> Toast.makeText(this,"Escolha uma opção acima",Toast.LENGTH_SHORT).show()
               }
           }
       }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_layout)
        bancoController=BancoController(this@MainActivity,"biblia",null,1)
        prefs = this.getPreferences(android.content.Context.MODE_PRIVATE)
        @Suppress
        editor = prefs.edit()
        switch_livros.setOnCheckedChangeListener(this)
        switch_ambos.setOnCheckedChangeListener(this)
        button.setOnClickListener(this)
        livros.put("Genesis",50)
        livros.put("Exodo",40)
        livros.put("Levitico",27)
        livros.put("Numeros",36)
        livros.put("Deuteronomio",34)
        livros.put("Josue",24)
        livros.put("Juizes",21)
        livros.put("Rute",4)
        livros.put("1 Samuel",31)
        livros.put("2 Samuel",24)
        livros.put("1 Reis",22)
        livros.put("2 Reis",25)
        livros.put("1 Cronicas",29)
        livros.put("2 Cronicas",36)
        livros.put("Esdras",10)
        livros.put("Neemias",13)
        livros.put("Ester",10)
        livros.put("Jo",42)
        livros.put("Salmos",150)
        livros.put("Proverbios",31)
        livros.put("Eclesiastes",12)
        livros.put("Canticos",8)
        livros.put("Isaias",66)
        livros.put("Jeremias",52)
        livros.put("Lamentacoes",5)
        livros.put("Ezequiel",48)
        livros.put("Daniel",12)
        livros.put("Oseias",14)
        livros.put("Joel",3)
        livros.put("Amos",9)
        livros.put("Obadias",1)
        livros.put("Jonas",4)
        livros.put("Miqueias",7)
        livros.put("Naum",3)
        livros.put("Habacuque",3)
        livros.put("Sofonias",3)
        livros.put("Ageu",2)
        livros.put("Zacarias",14)
        livros.put("Malaquias",4)
        livros.put("Mateus",28)
        livros.put("Marcos",16)
        livros.put("Lucas",24)
        livros.put("Joao",21)
        livros.put("Atos",28)
        livros.put("Romanos",16)
        livros.put("1 Corintios",16)
        livros.put("2 Corintios",13)
        livros.put("Galatas",6)
        livros.put("Efesios",6)
        livros.put("Filipenses",4)
        livros.put("Colossenses",4)
        livros.put("1 Tessalonicenses",5)
        livros.put("2 Tessalonicenses",3)
        livros.put("1 Timoteo",6)
        livros.put("2 Timoteo",4)
        livros.put("Tito",3)
        livros.put("Filemom",1)
        livros.put("Hebreus",13)
        livros.put("Tiago",5)
        livros.put("1 Pedro",5)
        livros.put("2 Pedro",3)
        livros.put("1 Joao",5)
        livros.put("2 Joao",1)
        livros.put("3 Joao",1)
        livros.put("Judas",1)
        livros.put("Apocalipse",22)
        listaLivros.addAll(livros.keys)
    }

    override fun onPause() {
        super.onPause()
        editor.putString("livro",result_livro!!.text.toString())
        editor.putString("capitulo",result_capitulo!!.text.toString())
        editor.commit()
    }

    override fun onResume() {
        super.onResume()
        result_livro!!.text = prefs.getString("livro",null)
        result_capitulo!!.text = prefs.getString("capitulo",null)
    }
}
