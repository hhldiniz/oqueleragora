package com.example.hugo.oqueleragora

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_passagem.*
import kotlinx.android.synthetic.main.content_scrolling.*

class PassagemActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_passagem)
        val fabparent = fab.parent as ViewGroup
        fabparent.removeView(fab)
        if(this.intent.getIntExtra("capitulo",0)!=0)
        {
            toolbar.title="${this.intent.getStringExtra("livro")} ${this.intent.getIntExtra("capitulo",0)}"
        }
        else
        {
            toolbar.title= this.intent.getStringExtra("livro")
        }
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.removeView(fab)
        texto_passagem.text = this.intent.getStringExtra("verso")
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}
