package com.example.hugo.oqueleragora

import android.content.Context
import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.io.BufferedReader
import java.io.InputStreamReader




/**
* Created by hugo on 07/06/17.
*/
class BancoController(c : Context, nome_db : String, cursor: SQLiteDatabase.CursorFactory?,db_version : Int) : SQLiteOpenHelper(c,nome_db,cursor,db_version){
    private val inputStream = c.resources.openRawResource(R.raw.aa)!!
    private val insertReader: BufferedReader = BufferedReader(InputStreamReader(inputStream))
    private val context: Context = c
    private val intent: Intent
    init {
        intent = Intent(context,PassagemActivity::class.java)
    }
    override fun onCreate(db: SQLiteDatabase?) {
        while (insertReader.ready()) {
            val insertStmt = insertReader.readLine()
            db?.execSQL(insertStmt)
        }
        insertReader.close()
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.rawQuery("DROP TABLE IF EXISTS book",null).close()
        db.rawQuery("DROP TABLE IF EXISTS metadata",null).close()
        db.rawQuery("DROP TABLE IF EXISTS testament",null).close()
        db.rawQuery("DROP TABLE IF EXISTS verse",null).close()
    }

    val abrirPassagemTodos = {livro : String, capitulo : Int ->
        val database = this.readableDatabase
        val result = database?.rawQuery("SELECT * FROM 'verse' WHERE book_id=(SELECT id FROM 'book' WHERE name='$livro') AND chapter=$capitulo",null)
        var textoPassagem = ""
        while(result!!.moveToNext())
        {
            textoPassagem += result.getString(3) + " " + result.getString(4) + "\n"
        }
        result.close()
        val intent = Intent(context,PassagemActivity::class.java)
        intent.putExtra("verso", textoPassagem)
        intent.putExtra("livro",livro)
        intent.putExtra("capitulo",capitulo)
        context.startActivity(intent)
    }

    val abrirPassagemLivro = {livro : String ->
        val database = this.readableDatabase
        val result = database?.rawQuery("SELECT * FROM 'verse' WHERE book_id=(SELECT id FROM 'book' WHERE name='$livro')",null)
        var textoPassagem = ""
        while(result!!.moveToNext())
        {
            textoPassagem += if (result.getInt(3) == 1) {
                "CAPÍTULO " + result.getString(2) + "\n" + result.getString(3) + " " + result.getString(4) + "\n"
            } else {
                result.getString(3) + " " + result.getString(4) + "\n"
            }
        }
        result.close()
        intent.putExtra("verso", textoPassagem)
        intent.putExtra("livro",livro)
        context.startActivity(intent)
    }
}